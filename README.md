# Wordpress REST API v2プラグインを使った各機能の練習フォルダ #

## 使い方 ##
* [Wordpress REST API](http://ja.wp-api.org/)を使用しているWordpressにインストールする
* 本リポジトリをwp-content/内に展開
* 「TwentySeventeen」の子テーマを有効化
* 「Wordpress REST API v2 Custom」プラグインを有効化

## 便利な使い方 ##

コミット毎に機能を随時追加していくためコミットを追っていけば色々な使い方が分かると思います

## 何かあれば ##

本リポジトリは僕の練習用のため動作を保証するものではなく、責任も負いません。ですが、感想やこうしたほうがいいよなどありましたら[@yhei_hei](https://twitter.com/yhei_hei)にお声がけください
