<?php
/**
* @package Wordpress_REST_API_Custom_Plugin
* @version 1.0
*/
/*
Plugin Name: Wordpress REST API v2 Custom
Plugin URI: http://rrr-z.jp
Description: テスト用にWordpress REST API プラグインを使った色々な機能を実装する
Author: Yohei Kokubo
Version: 1.0
Author URI: http://rrr-z.jp
*/

// Wordpress REST API プラグインが無効の場合は機能を抑止する
if (!function_exists('register_rest_route')) {
  return;
}

/*
 エンドポイントを追加するテスト
 WordpressのURL/wp-json/wrac_test/v1/added_test にブラウザでアクセスすると確認できる
*/
function my_add_endpoint_test_func( $data ) {
    return "hoge-!!";
}

add_action( 'rest_api_init', function () {
  // URLの指定とGETされた時に呼ばれる関数の定義
    register_rest_route( 'wrac_test/v1', '/added_test', array(
        'methods' => 'GET',
        'callback' => 'my_add_endpoint_test_func',
    ) );
} );


/*
 POSTのエンドポイントを追加するテスト
 WordpressのURL/wp-json/wrac_test/v1/rest_tests/add にpostするとテーブルにレコードが挿入される
*/

function my_add_endpoint_test_post_func() {
  //$data = $_POST; // POSTのパラメータを受け取りたい場合は$_POSTが使える
  // $data = array( 'some', 'response', 'data' );
  
  // POSTのパラメータがなければBad Request(本当はもっと厳密)
  if (empty($_POST)) {
    $response = new WP_REST_Response("parameter error");
    $response->set_status( 400 );
    $domain = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"];
    $response->header( 'Location', $domain );
  
    return $response;
  }
  
  // 特定テーブルにレコードを挿入する
  global $wpdb;
  $table_name = $wpdb->prefix . "rest_test";
  $result = $wpdb->insert( $table_name, array(
       'name' => $_POST['name'],
       'text' => $_POST['text'],
       'url' => $_POST['url'],
  ), array('%s', '%s', '%s',) );
  
  $response = new WP_REST_Response( $result );
  $response->set_status( 201 );
  $domain = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"];
  $response->header( 'Location', $domain );

  return $response;
}

add_action( 'rest_api_init', function () {
  // URLの指定とPOSTされた時に呼ばれる関数の定義
    register_rest_route( 'wrac_test/v1', '/rest_tests/add', array(
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'my_add_endpoint_test_post_func',
    ) );
} );


/*
 GETのエンドポイントを追加しurlに付与されたidから特定テーブルにアクセスしデータを取得する
 WordpressのURL/wp-json/wrac_test/v1/rest_tests/<id> でブラウザアクセスするとそのidに応じたデータが取れる
 idを付与しない場合全てのレコードが取れる
*/

function my_add_endpoint_test_get_func( $data ) {
  
  global $wpdb;
  $table_name = $wpdb->prefix . "rest_test";
  
  if( !empty($data['id']) ) {
    // 特定IDのレコードを取得
    $prepared_sql = $wpdb->prepare("
			SELECT *
			FROM $table_name
			WHERE ID = %s
			",
			$data['id']
		);
		$rest_test = $wpdb->get_results($prepared_sql);
		return $rest_test;
  } else {
    // IDが指定されていなければ全レコードを返却
    $sql = "SELECT * FROM " . $table_name;
    $rest_tests = $wpdb->get_results($sql);
    return $rest_tests;
  }
}

add_action( 'rest_api_init', function () {
  // URLの指定とGETされた時に呼ばれる関数の定義
    register_rest_route( 'wrac_test/v1', '/rest_tests/(?P<id>\d+)', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'my_add_endpoint_test_get_func',
    ) );
} );
add_action( 'rest_api_init', function () {
  // URLの指定とGETされた時に呼ばれる関数の定義(IDなしの場合)
    register_rest_route( 'wrac_test/v1', '/rest_tests', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'my_add_endpoint_test_get_func',
    ) );
} );


/*
 パーミッションチェックをする実装
 POSTする際にヘッダに正しいTest-Tokenを付与しないと403エラーになる
*/

function is_permission() {
  $access_token = $_SERVER['HTTP_TEST_TOKEN'];
  if($access_token != 'hoge' ) {
    return false;
  }
  return true;
}

function my_permission_test_func( $data ) {
    return "permission check success!";
}

add_action( 'rest_api_init', function () {
  // URLの指定とPOSTされた時に呼ばれる関数の定義
    register_rest_route( 'wrac_test/v1', '/permission', array(
        'methods' => WP_REST_Server::READABLE,
        'permission_callback' => 'is_permission', // is_permission関数で権限をチェックする
        'callback' => 'my_permission_test_func',
    ) );
} );


/*
 プラグインを有効化に動作させる処理のテスト
 有効化した時に一度だけ変な文字が出てます的アラートが出る。echoはダメらしい
*/

function test_plugin_init(){
  // プラグイン有効化時に１度だけ実施したい処理を書く
  // echo("hogehogehoge");

  // プラグイン有効化時にwp_rest_testテーブルを作る
  global $wpdb;
  $table_name = $wpdb->prefix . "rest_test"; 
  $charset_collate = $wpdb->get_charset_collate();
  
  $sql = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    name tinytext NOT NULL,
    text text NOT NULL,
    url varchar(55) DEFAULT '' NOT NULL,
    UNIQUE KEY id (id)
  ) $charset_collate;";
  
  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  dbDelta( $sql );
}

// add_action( 'plugins_loaded', 'test_plugin_init' );
register_activation_hook( __FILE__, 'test_plugin_init' );

